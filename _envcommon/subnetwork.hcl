terraform {
  source = "${local.base_source_url}"
}

locals {
  environment_vars = read_terragrunt_config(find_in_parent_folders("environment.hcl"))
  region_vars      = read_terragrunt_config(find_in_parent_folders("region.hcl"))

  env           = local.environment_vars.locals.environment_name
  region        = local.region_vars.locals.region
  ip_cidr_range = local.region_vars.locals.ip_cidr_range

  base_source_url = "git@bitbucket.org:not_vainglorious/modules.git//subnetwork"
}


inputs = {
  name          = "subnetwork-${local.env}"
  region        = local.region
  ip_cidr_range = local.ip_cidr_range
}
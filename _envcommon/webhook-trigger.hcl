terraform {
  source = "${local.base_source_url}"
}

locals {
  environment_vars = read_terragrunt_config(find_in_parent_folders("environment.hcl"))
  app_vars         = read_terragrunt_config(find_in_parent_folders("application.hcl"))

  env             = local.environment_vars.locals.environment_name
  repo_name       = local.app_vars.locals.repo_name
  csr_repo_name   = local.app_vars.locals.csr_repo_name
  branch_name     = local.app_vars.locals.branch_name

  base_source_url = "git@bitbucket.org:not_vainglorious/modules.git//webhook-trigger"
}


inputs = {
  name             = "webhook-${local.env}-${local.repo_name}"
  environment_name = local.env
  repo_name        = local.repo_name
  csr_repo_name    = local.csr_repo_name
  branch_name      = local.branch_name
}
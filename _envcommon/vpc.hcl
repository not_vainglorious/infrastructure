terraform {
  source = "${local.base_source_url}"
}

locals {
  environment_vars = read_terragrunt_config(find_in_parent_folders("environment.hcl"))

  env    = local.environment_vars.locals.environment_name
  region = local.environment_vars.locals.default_shared_resources_region

  base_source_url = "git@bitbucket.org:not_vainglorious/modules.git//vpc"
}


inputs = {
  name   = "vpc-${local.env}"
  region = local.region
}
terraform {
  source = "${local.base_source_url}"
}

locals {
  environment_vars = read_terragrunt_config(find_in_parent_folders("environment.hcl"))
  app_vars         = read_terragrunt_config(find_in_parent_folders("application.hcl"))

  env       = local.environment_vars.locals.environment_name
  repo_name = local.app_vars.locals.repo_name

  base_source_url = "git@bitbucket.org:not_vainglorious/modules.git//sql-database"
}


inputs = {
  name = "sql-database-${local.env}-${local.repo_name}"
}
terraform {
  source = "${local.base_source_url}"
}

locals {
  environment_vars = read_terragrunt_config(find_in_parent_folders("environment.hcl"))
  region_vars      = read_terragrunt_config(find_in_parent_folders("region.hcl"))
  application_vars = read_terragrunt_config(find_in_parent_folders("application.hcl"))

  env             = local.environment_vars.locals.environment_name
  region          = local.region_vars.locals.region
  repo_name       = local.application_vars.locals.repo_name

  base_source_url = "git@bitbucket.org:not_vainglorious/modules.git//cloud-run-server"
}


inputs = {
  name             = "cloud-run-${local.env}-${local.repo_name}"
  region           = local.region
}
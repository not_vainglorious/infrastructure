locals {
  
  env_vars = read_terragrunt_config(find_in_parent_folders("environment.hcl", "environment.hcl"), {})

  region_vars = read_terragrunt_config(find_in_parent_folders("region.hcl", "region.hcl"), {})

  app_vars = read_terragrunt_config(find_in_parent_folders("application.hcl", "application.hcl"), {})
}

generate "provider" {
  path      = "provider.tf"
  if_exists = "overwrite_terragrunt"
  contents  = <<EOF
provider "google" {
  project = var.project
}
EOF
}

remote_state {
  backend = "gcs"

  config = {
    project        = get_env("TF_VAR_project")
    bucket         = "guarding-athena-terragrunt-state"
    location       = "europe-west3"
    prefix         = "${path_relative_to_include()}"
  }

  generate = {
    path      = "backend.tf"
    if_exists = "overwrite_terragrunt"
  }
}

inputs = merge(
  local.env_vars,
  local.region_vars,
  local.app_vars
)
locals {
    environment_name                = "staging"
    default_shared_resources_region = "europe-west3"
}
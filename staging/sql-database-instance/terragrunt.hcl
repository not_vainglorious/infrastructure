locals {
  project = get_env("TF_VAR_project")
}

include "root" {
  path = find_in_parent_folders()
}

include "envcommon" {
  path = "${dirname(find_in_parent_folders())}/_envcommon/sql-database-instance.hcl"
}

dependency "vpc" {
  config_path = find_in_parent_folders("vpc")

  mock_outputs = {
    name = "temp-name"
  }
}

inputs = {
  network            = dependency.vpc.outputs.name
  tier               = "db-f1-micro"
  database_version   = "POSTGRES_14"
}
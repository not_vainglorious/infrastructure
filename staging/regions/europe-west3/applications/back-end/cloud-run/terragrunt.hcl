include "root" {
  path = find_in_parent_folders()
}

locals {
  environment_vars = read_terragrunt_config(find_in_parent_folders("environment.hcl"))

  env = local.environment_vars.locals.environment_name
}

include "envcommon" {
  path = "${dirname(find_in_parent_folders())}/_envcommon/cloud-run-server.hcl"
}

dependency "vpc" {
  config_path = find_in_parent_folders("vpc")

  mock_outputs = {
    name                 = "temp-name"
    vpc_access_connector = "temp-vpc-access-connector"
  }
}

dependency "cloud_build" {
  config_path = find_in_parent_folders("projects/back-end/cloud-build-trigger")

  mock_outputs = {
    id    = "temp-id"
    image = "temp-image"
  }
}

dependency "database" {
  config_path = find_in_parent_folders("projects/back-end/sql-database")

  mock_outputs = {
    secret_name = "temp-secret-name"
  }
}

dependency "redis" {
  config_path = find_in_parent_folders("memorystore")

  mock_outputs = {
    secret_name = "temp-secret-name"
  }
}

inputs = {
  network           = dependency.vpc.outputs.name
  container_command = ["npm", "run", "start"]
  image             = dependency.cloud_build.outputs.image
  port              = 3000
  envs              = { "POSTGRES_URL": dependency.database.outputs.secret_name, "REDIS_URL": dependency.redis.outputs.secret_name }
  vpc_connector     = dependency.vpc.outputs.vpc_access_connector
  domain_name       = "api.${local.env}.${get_env("DOMAIN_NAME")}"
}
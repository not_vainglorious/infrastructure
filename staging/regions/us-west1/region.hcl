locals {
  location      = "us"
  region        = "us-west1"
  ip_cidr_range = "10.1.0.0/16"
}
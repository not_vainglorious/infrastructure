locals {
  project = get_env("TF_VAR_project")
}

include "root" {
  path = find_in_parent_folders()
}

include "envcommon" {
  path = "${dirname(find_in_parent_folders())}/_envcommon/sql-database.hcl"
}

dependency "vpc" {
  config_path = find_in_parent_folders("vpc")

  mock_outputs = {
    name = "temp-name"
  }
}

dependency "database_instance" {
  config_path = find_in_parent_folders("sql-database-instance")

  mock_outputs = {
    name       = "temp-name"
    connection = "temp-connection"
  }
}

inputs = {
  network             = dependency.vpc.outputs.name
  instance_name       = dependency.database_instance.outputs.name
  instance_driver     = "postgres"
  instance_connection = dependency.database_instance.outputs.connection
  instance_port       = 5432
}
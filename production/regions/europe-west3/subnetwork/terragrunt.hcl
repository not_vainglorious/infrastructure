include "root" {
  path = find_in_parent_folders()
}

include "envcommon" {
  path = "${dirname(find_in_parent_folders())}/_envcommon/subnetwork.hcl"
}

dependency "vpc" {
  config_path = find_in_parent_folders("vpc")

  mock_outputs = {
    name      = "temp-name"
  }
}

inputs = {
  network = dependency.vpc.outputs.name
}
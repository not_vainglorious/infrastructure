locals {
  region = "europe-west3"
  ip_cidr_range = "10.13.0.0/16"
}